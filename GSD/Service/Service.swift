//
//  Service.swift
//  GSD
//
//  Created by Pawel Kacela on 07/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit
import Alamofire

enum EndPoint: String {
    case UserList = "users"
    case Search = "search/users"
}

struct Service {
    
    enum ConnectionError: String {
        case TryAgain = "Coś poszło nie tak, spróbuj ponownie za kilka minut"
    }
    
    static let sharedInstance = Service()
    
    let baseUrl = "https://api.github.com"
    
    func getApi<T: Decodable>(completion: @escaping (T?, _ err: String?) -> (), endpoint: String, parameters: [String: Any]? ){
        let urlString = "\(baseUrl)/\(endpoint)"
        AF.request(urlString, method: .get, parameters: parameters)
            .responseJSON { response in
                print(response.response?.statusCode as Any)
                if response.data != nil {
                    do {
                        switch response.response?.statusCode {
                        case 200:
                            if let result = response.data {
                                print(response)
                                let obj = try JSONDecoder().decode(T.self, from: result)
                                completion(obj, nil)
                            }
                        case 502:
                            completion(nil, ConnectionError.TryAgain.rawValue)
                        default:
                            if let result = response.data {
                                let errMsg = try JSONDecoder().decode(ErrorResponse.self, from: result)
                                completion(nil, errMsg.message)
                            }
                        }
                    }
                    catch {
                        completion(nil, error.localizedDescription)
                    }
                }
        }
    }
    
}


