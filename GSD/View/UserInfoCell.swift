//
//  UserInfoCell.swift
//  GSD
//
//  Created by Pawel Kacela on 08/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

class UserInfoCell: UICollectionViewCell {
    
    var userInfo: UserInfo? {
        didSet {
            guard let unwrappedUserInfo = userInfo else { return }
            titleLabeL.text = "\(unwrappedUserInfo.title):"
            valueLabel.text  = "\(unwrappedUserInfo.value ?? "Brak informacji")"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private let titleLabeL: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        return label
    }()
    
    private let valueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        return label
    }()
    
    private func setupViews(){
        backgroundColor = .white
        addSubview(titleLabeL)
        addSubview(valueLabel)
        
        addConstraintsWithFormat(format: "H:|-32-[v0]", views: titleLabeL)
        addConstraintsWithFormat(format: "H:|-32-[v0]", views: valueLabel)
        addConstraintsWithFormat(format: "V:|-16-[v0]-10-[v1]", views: titleLabeL, valueLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
