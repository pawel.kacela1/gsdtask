//
//  UserCell.swift
//  GSD
//
//  Created by Pawel Kacela on 07/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

class UserNameCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    var user: User? {
        didSet {
            guard let unwrappedUser = user else { return }
            nameLabel.text = "\(unwrappedUser.login ?? "brak")"
        }
    }

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Login:"
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        return label
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return label
    }()
    
    private let rightArrowImg: UIImageView = {
        let imager = UIImageView()
        imager.image = UIImage(systemName: "chevron.right")
        imager.contentMode = ContentMode.scaleAspectFit
        imager.tintColor = .black
        return imager
    }()
    
    private func setupViews(){
        backgroundColor = .white
        
        addSubview(nameLabel)
        addSubview(titleLabel)
        addSubview(rightArrowImg)
        addConstraintsWithFormat(format: "H:|-16-[v0]-8-[v1]", views: titleLabel, nameLabel)
        addConstraintsWithFormat(format: "H:[v0]-16-|", views: rightArrowImg)
        rightArrowImg.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
