//
//  AvatarCell.swift
//  GSD
//
//  Created by Pawel Kacela on 08/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

class AvatarCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    var userDetails: UserDetails? {
        didSet {
            guard let unwrappedUserDetails = userDetails else { return }
            guard let avatarUrl = unwrappedUserDetails.avatar_url else { return }
            userAvatarImageView.sdSetImage(urlString: avatarUrl)
        }
    }
    
     private let userAvatarImageView: UIImageView = {
        let imager = UIImageView()
        imager.contentMode = ContentMode.scaleAspectFit
        imager.layer.cornerRadius = 50
        imager.layer.borderColor = UIColor.black.cgColor
        imager.layer.borderWidth = 1
        imager.layer.masksToBounds = true
        return imager
    }()
    
    private func setupViews(){
        backgroundColor = .white        
        addSubview(userAvatarImageView)
        addConstraintsWithFormat(format: "H:[v0(100)]", views: userAvatarImageView)
        addConstraintsWithFormat(format: "V:|-32-[v0(100)]", views: userAvatarImageView)
        userAvatarImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
