//
//  QualifierInputCell.swift
//  GSD
//
//  Created by Pawel Kacela on 21/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

class QualifierInputCell: UICollectionViewCell {
    
    var myCollectionView: SearchViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    var qualifier: String? {
        didSet {
            guard let unwrappedQualifier = qualifier else { return }
            titleLabel.text = "\(unwrappedQualifier):"
            inputField.placeholder  = "wprowadź \(unwrappedQualifier.lowercased())"
        }
    }

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        return label
    }()
    
    let inputField: UITextField = {
        let label = UITextField()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.autocorrectionType = .no
        return label
    }()
    
    private func setupViews(){
        backgroundColor = .white
        addSubview(inputField)
        addSubview(titleLabel)
        
        inputField.addTarget(self, action: #selector(handleTextViewEditing), for: .allEditingEvents)

        addConstraintsWithFormat(format: "H:|-16-[v0]-8-[v1(200)]", views: titleLabel, inputField)
        addConstraintsWithFormat(format: "V:[v0(44)]", views: inputField)
        inputField.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    @objc func handleTextViewEditing(){
          myCollectionView?.saveEditedText(cell: self)
      }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

