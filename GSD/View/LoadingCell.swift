//
//  LoadingCell.swift
//  GSD
//
//  Created by Pawel Kacela on 07/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

class LoadingCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
 
    let indicatorView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .medium
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()
    
    let infoLabel: UILabel = {
        let label = UILabel()
        label.text = "Ładowanie..."
        return label
    }()
    
    private func setupViews(){
        backgroundColor = .white
        addSubview(indicatorView)
        addSubview(infoLabel)
        indicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        indicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        addConstraintsWithFormat(format: "H:|-16-[v0]", views: infoLabel)
        addConstraintsWithFormat(format: "V:|-16-[v0]", views: infoLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
