//
//  UserInfo.swift
//  GSD
//
//  Created by Pawel Kacela on 08/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

struct UserInfo {
    let title: String
    let value: Any?
}
