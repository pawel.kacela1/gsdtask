//
//  ErrorResponse.swift
//  GSD
//
//  Created by Pawel Kacela on 07/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

struct ErrorResponse: Decodable {
        let message: String?
}
