//
//  SearchResponse.swift
//  GSD
//
//  Created by Pawel Kacela on 21/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

struct SearchResponse: Decodable {
    let items: [User]?
}
