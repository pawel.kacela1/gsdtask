//
//  Users.swift
//  GSD
//
//  Created by Pawel Kacela on 07/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

struct User: Decodable {
    let login: String?
    let id: Int?
}

