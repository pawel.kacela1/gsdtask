//
//  UIImageViewExtension.swift
//  GSD
//
//  Created by Pawel Kacela on 08/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    func sdSetImage(urlString: String){
        sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "placeholder.png"))
        sd_setShowActivityIndicatorView(true)
        sd_setIndicatorStyle(.medium)
    }
}

