//
//  UIColorExtension.swift
//  GSD
//
//  Created by Pawel Kacela on 09/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
}

