//
//  Extension.swift
//  GSD
//
//  Created by Pawel Kacela on 07/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

extension UIView {
    
    func addConstraintsWithFormat(format: String, views: UIView...){
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated(){
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func showActivityIndicator(activityIndicator: UIActivityIndicatorView ){
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.medium
        activityIndicator.color = UIColor.black
        activityIndicator.backgroundColor = UIColor(r: 0, g: 0, b: 0, a: 0.2)
        addSubview(activityIndicator)
        addConstraintsWithFormat(format: "H:|[v0]|", views: activityIndicator)
        addConstraintsWithFormat(format: "V:|[v0]|", views: activityIndicator)
    }
    
    func showError(error: String){
         let errorView: UIView = {
             let view = UIView()
            view.backgroundColor = UIColor.red.withAlphaComponent(0.7)
             return view
         }()
        
         let errorMessage: UILabel = {
            let label = UILabel()
            label.textColor = .black
            label.text = error
            label.textAlignment = NSTextAlignment.center
            label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            label.numberOfLines = 2
            return label
         }()
         
         self.addSubview(errorView)
         self.addConstraintsWithFormat(format: "H:|[v0]|", views: errorView)
         self.addConstraintsWithFormat(format: "V:[v0(90)]|", views: errorView)
         errorView.addSubview(errorMessage)
         errorView.addConstraintsWithFormat(format: "H:|-16-[v0]-16-|", views: errorMessage)
         errorView.addConstraintsWithFormat(format: "V:|-16-[v0]-16-|", views: errorMessage)
         
         UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
             errorView.frame = CGRect(x: 0, y: -90, width: 0, height: 0)
         }, completion: nil)
     }
    
}
