//
//  MainSplitViewController.swift
//  GSD
//
//  Created by Pawel Kacela on 09/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

class MainSplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        configureSplitViewController()
        setViewControllers()
    }
    
    private func setViewControllers(){
        let masterController = UINavigationController(rootViewController: MasterViewController(collectionViewLayout: UICollectionViewFlowLayout()))
        let detailController = UINavigationController(rootViewController: UserDetailsViewController(collectionViewLayout: UICollectionViewFlowLayout()))
        viewControllers = [masterController, detailController]
    }
    
    private func configureSplitViewController(){
        maximumPrimaryColumnWidth = view.bounds.size.width
        preferredPrimaryColumnWidthFraction = 1/2
        preferredDisplayMode = .allVisible
        setOverrideTraitCollection(UITraitCollection(horizontalSizeClass: .compact), forChild: self)
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }
        
//    override func overrideTraitCollection(forChild childViewController: UIViewController) -> UITraitCollection? {
//        setOverrideTraitCollection(UITraitCollection(horizontalSizeClass: .compact), forChild: self)
//        return UITraitCollection(horizontalSizeClass: .compact)
//    }
//
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        setOverrideTraitCollection(UITraitCollection(horizontalSizeClass: .compact), forChild: self)
//    }
    
    
    
//    override func overrideTraitCollection(forChild childViewController: UIViewController) -> UITraitCollection? {
//        if UIDevice.current.userInterfaceIdiom != .pad {
//            return UITraitCollection(verticalSizeClass: .compact)
//        } else {
//            return super.traitCollection
//        }
//    }
    
    
}
