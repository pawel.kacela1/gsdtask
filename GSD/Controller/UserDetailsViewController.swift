//
//  UserDetailsViewController.swift
//  GSD
//
//  Created by Pawel Kacela on 07/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

class UserDetailsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var currentUser: User?
    let avatarCellId = "avatarCellId"
    let userInfoCellId = "userInfoCellId"
    var currentUserDetails: UserDetails?
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    enum SectionTypes: CaseIterable {
        case Avatar
        case UserInfo
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCells()
        setupViews()
        setupCollectionView()
        setNavigationBar()
        getApiUser()
    }
    
    private func setNavigationBar(){
        guard let unwrappedUser = currentUser else { return }
        navigationItem.title = unwrappedUser.login
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
        
    private func setupViews(){
        collectionView.backgroundColor = .white
        view.showActivityIndicator(activityIndicator: activityIndicator)
    }
    
    private func setupCollectionView(){
        collectionView?.contentInsetAdjustmentBehavior = .always
    }
    
    private func registerCells(){
        collectionView.register(AvatarCell.self, forCellWithReuseIdentifier: avatarCellId)
        collectionView.register(UserInfoCell.self, forCellWithReuseIdentifier: userInfoCellId)
    }
    
    private func getApiUser(){
        guard let unwrappedUserName = currentUser?.login else { return }
        activityIndicator.startAnimating()
        Service.sharedInstance.getApi(completion: { (userFeed: UserDetails?, err) in
            self.activityIndicator.stopAnimating()
            guard err == nil else {
                self.view.showError(error: err ?? "error")
                return}
            self.currentUserDetails = userFeed
            self.collectionView.reloadData()
        }, endpoint: "\(EndPoint.UserList.rawValue)/\(unwrappedUserName)", parameters: nil)
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return SectionTypes.allCases.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let sectionType = SectionTypes.allCases[section]
        switch sectionType {
        case .Avatar:
            return currentUserDetails == nil ? 0:1
        case .UserInfo:
            return currentUserDetails == nil ? 0:26
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let sectionType = SectionTypes.allCases[indexPath.section]
        
        switch sectionType {
        case .Avatar:
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: avatarCellId, for: indexPath) as! AvatarCell
            cell.userDetails = currentUserDetails
            return cell
        case .UserInfo:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: userInfoCellId, for: indexPath) as! UserInfoCell
            guard let info = getUserInfoForCell(index: indexPath.row) else {return cell}
            cell.userInfo = info
            return cell
        }
    }
    
    private func getUserInfoForCell(index: Int) -> UserInfo? {
        
        let UserInfoArray = [
                UserInfo(title: "Firma", value: currentUserDetails?.company),
                UserInfo(title: "Blog", value: currentUserDetails?.blog),
                UserInfo(title: "Utworzono", value: currentUserDetails?.created_at),
                UserInfo(title: "Email", value: currentUserDetails?.email),
                UserInfo(title: "Followers", value: currentUserDetails?.followers),
                UserInfo(title: "Following", value: currentUserDetails?.following),
                UserInfo(title: "Gits", value: currentUserDetails?.gists_url),
                UserInfo(title: "Gravatar id", value: currentUserDetails?.gravatar_id),
                UserInfo(title: "Do wynajęcia", value: currentUserDetails?.hireable),
                UserInfo(title: "Html url", value: currentUserDetails?.html_url),
                UserInfo(title: "id", value: currentUserDetails?.id),
                UserInfo(title: "Lokacja", value: currentUserDetails?.location),
                UserInfo(title: "Login", value: currentUserDetails?.login),
                UserInfo(title: "Nazwa", value: currentUserDetails?.name),
                UserInfo(title: "Node id", value: currentUserDetails?.node_id),
                UserInfo(title: "Url organizacji", value: currentUserDetails?.organizations_url),
                UserInfo(title: "Publiczne git'y", value: currentUserDetails?.public_gists),
                UserInfo(title: "Publiczne repozytoria", value: currentUserDetails?.public_gists),
                UserInfo(title: "Url otrzymanych wydarzeń", value: currentUserDetails?.received_events_url),
                UserInfo(title: "Url repozytorium", value: currentUserDetails?.repos_url),
                UserInfo(title: "Admin strony", value: currentUserDetails?.site_admin),
                UserInfo(title: "Ogwiazdkowene Url'e", value: currentUserDetails?.starred_url),
                UserInfo(title: "Subskrybowane Url'e", value: currentUserDetails?.subscriptions_url),
                UserInfo(title: "Typ", value: currentUserDetails?.type),
                UserInfo(title: "Aktualizowanuy", value: currentUserDetails?.updated_at),
                UserInfo(title: "Url", value: currentUserDetails?.url)
        ]
        
        return UserInfoArray[index]
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let sectionType = SectionTypes.allCases[indexPath.section]
        switch sectionType {
        case .Avatar:
            return CGSize(width: view.frame.width, height: 168)
        case .UserInfo:
            return CGSize(width: view.frame.width, height: 80)
        }
    }
    
}
