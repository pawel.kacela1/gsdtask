//
//  SearchViewController.swift
//  GSD
//
//  Created by Pawel Kacela on 21/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

protocol SearchUpdateDelegate {
    func updateSearchResult(searchQualifier: String)
}

class SearchViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let qualifierInputCellId = "qualifierInputCellId"
    var searchUpdateDelegate: SearchUpdateDelegate?
    
    var nameHolder: String?
    var emailHolder: String?
    var loginHolder: String?
    
    enum SearchQualifiers: CaseIterable {
        case name
        case email
        case login
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        registerCells()
        setupSearchButton()
        setNavigationBar()
        hideKeyboardWhenTappedAround()
    }
    
    private func setupViews(){
        collectionView.backgroundColor = .lightGray
    }
    
    private func setNavigationBar(){
        navigationItem.title = "Szukaj"
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
    
    private func registerCells(){
        collectionView.register(QualifierInputCell.self, forCellWithReuseIdentifier: qualifierInputCellId)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SearchQualifiers.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: qualifierInputCellId, for: indexPath) as! QualifierInputCell
        let searchQualifer = SearchQualifiers.allCases[indexPath.row]
        cell.myCollectionView = self
        
        switch searchQualifer {
        case .name:
            cell.qualifier = "Imię"
        case .email:
            cell.qualifier = "Email"
        case .login:
            cell.qualifier = "Login"
        }
        
        return cell
    }
    
    private func setupSearchButton() {
        view.addSubview(searchButton)
        searchButton.addTarget(self, action: #selector(handleSearchButtonTap), for: .touchUpInside)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: searchButton)
        view.addConstraintsWithFormat(format: "V:[v0(80)]|", views: searchButton)
    }
    
    func saveEditedText(cell: UICollectionViewCell){
        
        if let indexPath = collectionView?.indexPath(for: cell){
            let searchQualifer = SearchQualifiers.allCases[indexPath.row]
            let textCell = cell as! QualifierInputCell
            let editedText = textCell.inputField.text ?? ""
            
            switch searchQualifer {
            case .name:
                nameHolder = editedText
            case .email:
                emailHolder = editedText
            case .login:
                loginHolder = editedText
            }
        }
        
        changeSearchButtonStatus()
    }
    
    private func changeSearchButtonStatus(){
        searchButtonActive()
        guard nameHolder == nil || nameHolder == "" else { return }
        guard emailHolder == nil || emailHolder == "" else { return }
        guard loginHolder == nil || loginHolder == "" else { return }
        searchButtonInactive()
    }
    
    private func searchButtonActive(){
        searchButton.backgroundColor = UIColor(r: 66, g: 194, b: 100, a: 1)
        searchButton.isUserInteractionEnabled = true
    }
    
    private func searchButtonInactive(){
        searchButton.isUserInteractionEnabled = false
        searchButton.backgroundColor = UIColor(r: 66, g: 194, b: 100, a: 0.5)
    }
    
    @objc private func handleSearchButtonTap(){
        var qualifier = ""
        
        if nameHolder != nil && nameHolder != "" {qualifier = qualifier + "\(nameHolder!) in:name "}
        if emailHolder != nil && emailHolder != "" {qualifier = qualifier + "\(emailHolder!) in:email "}
        if loginHolder != nil && loginHolder != "" {qualifier = qualifier + "\(loginHolder!) in:login"}
        
        searchUpdateDelegate?.updateSearchResult(searchQualifier: qualifier)
        navigationController?.popToRootViewController(animated: true)
    }
    
    let searchButton: UIButton = {
        let button = UIButton()
        button.setTitle("Szukaj", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        button.isUserInteractionEnabled = false
        button.backgroundColor = UIColor(r: 66, g: 194, b: 100, a: 0.5)
        return button
    }()
    
}
