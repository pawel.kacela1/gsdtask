//
//  HomeViewController.swift
//  GSD
//
//  Created by Pawel Kacela on 07/03/2020.
//  Copyright © 2020 Pawel Kacela. All rights reserved.
//

import UIKit

class MasterViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    let userNameCellId = "UserNameCellId"
    let loadingCellId = "LoadingCellId"
    var usersList = [User]()
    var isFetching = false
    var shouldFetch = true
    var currentSearchQualifier:String? = nil
    var currentPage = 3
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    enum SectionTypes: CaseIterable {
        case User
        case Loading
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        registerCells()
        fetchUsersList()
        setNavigationBar()
        setupSearchButton()
    }
    
    private func setupSearchButton(){
        let barButtonRightItem = UIBarButtonItem(title: "Szukaj", style: .done, target: self, action: #selector(handleSearchButtonTap))
        barButtonRightItem.tintColor = .black
        barButtonRightItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: .regular)], for: .normal)
        navigationItem.rightBarButtonItem = barButtonRightItem
    }
    
    @objc private func handleSearchButtonTap(){
        let desVC = SearchViewController(collectionViewLayout: UICollectionViewFlowLayout())
        desVC.searchUpdateDelegate = self
        navigationController?.pushViewController(desVC, animated: true)
    }
    
    private func initialSelctionForIPad(){
        guard UIDevice.current.userInterfaceIdiom == .pad else { return }
        guard !usersList.isEmpty else { return }
        
        let desVC = UserDetailsViewController(collectionViewLayout: UICollectionViewFlowLayout())
        desVC.currentUser = usersList[0]
        let navDesVC = UINavigationController(rootViewController: desVC)
        showDetailViewController(navDesVC, sender: nil)
    }
    
    private func searchUser(qualifiers: String){
        isFetching = true
        activityIndicator.startAnimating()
        Service.sharedInstance.getApi(completion: { (searchFeed: SearchResponse?, err) in
            self.activityIndicator.stopAnimating()
            guard err == nil else {
                self.view.showError(error: err ?? "error")
                return
            }
            guard let unwrappedUserList = searchFeed?.items else { return }
            guard !unwrappedUserList.isEmpty else {
                self.shouldFetch = false
                return
            }
            self.usersList = self.usersList + unwrappedUserList
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.isFetching = false
                self.initialSelctionForIPad()
            }
        }, endpoint: EndPoint.Search.rawValue, parameters: ["q": "\(qualifiers)", "page":currentPage])
    }
    
    private func setupCollectionView(){
        view.showActivityIndicator(activityIndicator: activityIndicator)
        collectionView.backgroundColor = .lightGray
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
    }
    
    private func registerCells(){
        collectionView.register(UserNameCell.self, forCellWithReuseIdentifier: userNameCellId)
        collectionView.register(LoadingCell.self, forCellWithReuseIdentifier: loadingCellId)
    }
    
    private func setNavigationBar(){
        navigationItem.title = "Lista użytkowników"
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
    
    private func fetchUsersList(){
        isFetching = true
        Service.sharedInstance.getApi(completion: { (usersFeed: [User]?, err) in
            guard err == nil else {
                self.view.showError(error: err ?? "error")
                return
            }
            guard let unwrappedUserList = usersFeed else { return }
            guard !unwrappedUserList.isEmpty else {
                self.shouldFetch = false
                return
            }
            self.usersList = self.usersList + unwrappedUserList
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.isFetching = false
                self.initialSelctionForIPad()
            }
        }, endpoint: EndPoint.UserList.rawValue, parameters: ["since": usersList.last?.id?.description ?? "0"])
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return SectionTypes.allCases.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        let sectionType = SectionTypes.allCases[section]
    
        switch sectionType {
        case .User:
            return usersList.count
        case .Loading:
            return isFetching ? 1:0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sectionType = SectionTypes.allCases[indexPath.section]
        guard sectionType == .User else { return }
        let desVC = UserDetailsViewController(collectionViewLayout: UICollectionViewFlowLayout())
        desVC.currentUser = usersList[indexPath.row]
        splitViewController?.showDetailViewController(desVC, sender: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let sectionType = SectionTypes.allCases[indexPath.section]
        
        switch sectionType {
        case .User:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: userNameCellId, for: indexPath) as! UserNameCell
            cell.user = usersList[indexPath.row]
            return cell
        case .Loading:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loadingCellId, for: indexPath) as! LoadingCell
            cell.indicatorView.startAnimating()
            return cell
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height * 1 {
            guard !isFetching && shouldFetch else { return }
            
            if currentSearchQualifier != nil {
                currentPage = currentPage + 1
                searchUser(qualifiers: currentSearchQualifier!)
            }
            else{
                fetchUsersList()
            }
            
            guard !usersList.isEmpty else { return }
            collectionView.reloadSections(IndexSet(integer: 1))
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.collectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    private func isPortait() -> Bool {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        return keyWindow?.windowScene?.interfaceOrientation.isPortrait ?? true
    }
    
}

extension MasterViewController: SearchUpdateDelegate {
    func updateSearchResult(searchQualifier: String) {
        guard searchQualifier != "" else { return }
        usersList.removeAll()
        currentSearchQualifier = searchQualifier
        searchUser(qualifiers: searchQualifier)
    }
}
